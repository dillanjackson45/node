SOCKS5 Proxy Package for Go
===========================

Documentation: <http://godoc.org/gitlab.com/parallelcoin/node/socks>

License
-------

3-clause BSD. See LICENSE file.
