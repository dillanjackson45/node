package memdb

import (
	"testing"

	"gitlab.com/parallelcoin/node/goleveldb/leveldb/testutil"
)

func TestMemDB(t *testing.T) {
	testutil.RunSuite(t, "MemDB Suite")
}
